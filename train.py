import random

class Perceptron:

    def __init__(self, numberOfInputs, learningRate=0.00001) -> None:
        self.no = numberOfInputs
        self.lernrc = learningRate
        self.inputs = []
        self.weights = []
        self.bias = 1
        self.name = "meysam"

    def activate(self, treshold):
        sum = 0
        for i in range(self.no):
            self.inputs.append(random.randint(0, 9))
            self.weights.append(random.random())
            sum += self.inputs[i] * self.weights[i]

        print('inputs {}'.format(self.inputs))
        print('weights {}'.format(self.weights))
        print('sum: {}'.format(sum))
        return sum >= treshold
            

perceptron = Perceptron(10)
result = perceptron.activate(20)
print(result)
